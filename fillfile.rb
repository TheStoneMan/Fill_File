#!/usr/bin/env ruby

require 'optparse'
require 'ostruct'

def read_all_bytes(file_path)
    file = File.open(file_path, "rb")     #Open the file as binary read.
    bytes = Array.new(file.size)   #Create a new array the size of the file. Will be populated with the file bytes.
    file.each_byte.with_index { |byte, index| bytes[index] = byte }  #Enumerate other the file's bytes, add them to the array.
    bytes
end

$TYPES = %w(byte half word)

def parse_arguments()
  options = OpenStruct.new
  options.nb_fills = 10
  options.value = 0
  options.type = $TYPES[0]
  options.position = 0
  options.path = nil
  options.bytes = []
  parser = OptionParser.new do |opts|
    opts.banner = "Usage: fillfile [options]"

    opts.on("-f", "--file FILEPATH", "A path to a readable file") do |path|
      options.bytes = read_all_bytes path
      options.path = path
    end

    opts.on("-s", "--size NB_FILLS", OptionParser::DecimalInteger, "The number of bytes/halfwords/words to replace (default: 10)") do |nb_fills|
      if (nb_fills < 0)
        abort("The number of fills must be positive")
      end
      options.nb_fills = nb_fills
    end

    opts.on("-v", "--value INTEGER", OptionParser::OctalInteger, "The unsigned value which can be decimal, octal or hexadecimal. Truncated to fit the size (default: 0)") do |value|
      options.value = value
    end

    opts.on("-t", "--type INTTYPE", $TYPES, "The type of value that will be filled. Little-endian. (default: byte)") do |type|
      options.type = type
    end

    opts.on("-p", "--position POS", OptionParser::OctalInteger, "The position of the first byte to fill (default: 0)") do |position|
      options.position = position
    end

    opts.on_tail("-h", "--help", "Show this message") do
        puts opts
        exit
    end

  end
  parser.parse!(ARGV)
  options.value = truncate_value(options.value, options.type)
  if (options.path == nil)
    abort("Error: Please provide a file path! (pass -h for help)")
  end
  options
end

def truncate_value(value, type)
  ret_val = nil
  if type == $TYPES[0]
    ret_val = value & 0xFF
  elsif type == $TYPES[1]
    ret_val = value & 0xFFFF
  else
    ret_val = value & 0xFFFFFFFF
  end
  ret_val
end

def fill(bytes, type, value, position, nb_fills)
  bytes_nb_fills = nb_fills * (1 << $TYPES.find_index(type))
  if type == $TYPES[0]
    bytes.fill(value, position, nb_fills)
    return bytes
  elsif type == $TYPES[1]
    sub_array = [value & 0xFF, (value >> 8) & 0xFF]
  else
    sub_array = [value & 0xFF, (value >> 8) & 0xFF, (value >> 16) & 0xFF, (value >> 24) & 0xFF]
  end
  bytes[position, bytes_nb_fills] = sub_array * nb_fills
  bytes
end

def write_all_bytes(path, bytes)
  file = File.open(path, "wb")
  bytes.map {|byte| file.putc byte}
  file.close
  nil
end

def main()
  options = parse_arguments()
  bytes = fill(options.bytes, options.type, options.value, options.position, options.nb_fills)
  write_all_bytes(options.path, bytes)
end

main()
