Fill File
_________

Ruby 2.0 or higher

Allow to fill a file with predetermined bytes, or halfwords, or words, at certain positions.

Launch the file with --help for for information